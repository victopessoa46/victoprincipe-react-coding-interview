import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company, Person } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const INITIAL_PERSON: Pick<Person, 'name' | 'birthday' | 'gender' | 'phone'> = {
  name: '',
  gender: '',
  phone: '',
  birthday: '',
};

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const [isEditing, setIsEditing] = useState<Boolean>(false);
  const [isLoading, setIsLoading] = useState<Boolean>(false);
  const [person, setPerson] = useState<
    Pick<Person, 'name' | 'birthday' | 'gender' | 'phone'>
  >({ ...INITIAL_PERSON });

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const handleClickEditSave = async () => {
    setIsEditing((e) => !e);
    if (isEditing) {
      setIsLoading(true);
      await save(person);
      setIsLoading(false);
    }
  };

  const handleInputChange = (inputName: string) => (e: any) => {
    setPerson((p: Person) => ({ ...data, ...p, [inputName]: e.target.value }));
  };

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={handleClickEditSave}>
            {isEditing ? 'Save' : 'Edit'}
          </Button>,
        ]}
      >
        {!!data && !isEditing && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
            <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
            <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>
            <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
          </Descriptions>
        )}
        {!!data && isEditing && (
          <Descriptions size="small">
            {Object.keys(INITIAL_PERSON).map((k) => (
              <Descriptions.Item label={k} key={k}>
                <input
                  type="text"
                  name={k}
                  value={person[k]}
                  onChange={handleInputChange(k)}
                />
              </Descriptions.Item>
            ))}
          </Descriptions>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
